
var invitationFormElt = document.getElementById('invitationForm');
var outputMessageElt = document.getElementById('outputMessage');

outputMessageElt.style.display = 'none';

invitationFormElt.addEventListener('submit', function(e) {
	e.preventDefault();

	outputMessageElt.style.display = 'block';

	var dateInputElt = document.getElementById('dateInput');
	var emailsInputElt = document.getElementById('emailsInput');
	outputMessageElt.innerHTML = ''
	  + 'Formulaire soumis :<br>\n'
	  + '- invitationDate: ' + dateInputElt.value + '<br>\n'
	  + '- emailAddresses: ' + emailsInputElt.value + '<br>\n'
    ;
});