# Test ChooseMyCompany - dev front #

**Confidentiel - ne pas transmettre**


Pré-requis:
---
Un navigateur supportant javascript et un accès internet...


Contexte:
---
ChooseMyCompany est un site d'information sur les employeurs.
L'information disponible sur ChooseMyCompany provient de 2 sources :
- des réponses "spontanées" apportées par les visiteurs du site
- des réponses "sollicitées" récoltées par des enquêtes chez nos clients

Pour administrer une enquête, un client doit pouvoir inviter des participants (les salariés de son entreprise).
Cet ajout se fait à partir d'un formulaire disponible dans son backoffice.

Pour inviter des participants, il faut remplir au minimum :
- la date à laquelle les inviter
- les adresses mails des participants

C'est ce que permet de faire le formulaire dans le fichier index.html.

Un problème qui revient souvent est la difficulté pour les clients à formatter correctement la liste des adresses mails (qui sont souvent copiées de sources externes comme des fichiers Excel).


A réaliser en 1h30 / 2h :
---
- Faites évoluer le code (sans utiliser de librairie externe) pour permettre au client de saisir des adresses mails sans avoir à respecter le format attendu. L'application devra formatter automatiquement le contenu saisi par l'utilisateur pour que la soumission du formulaire envoie des données correctes. Des exemples de saisies possibles sont donnés dans les fichiers data-sample/*.
- Intégrer en respectant la philosphie bootstrap le design de la maquette fournie (design.png)
- Si vous aviez plus de temps, quelles seraient les évolutions que vous proposeriez pour améliorer ce formulaire (ergonomie, organisation du code, sécurité, utilisation de librairies spécialisées...) ?